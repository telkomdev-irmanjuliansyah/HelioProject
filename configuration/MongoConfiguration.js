var mongoose = require('mongoose');
var dbURI = 'mongodb://irman:irman@ds117156.mlab.com:17156/heliodb';

var db = mongoose.connection;
db.on('connecting', function() {});

db.on('error', function(error) {
  mongoose.disconnect();
});
db.on('connected', function() {});
db.once('open', function() {});
db.on('reconnected', function() {});
db.on('disconnected', function() {
  mongoose.connect(dbURI,
    {
      useMongoClient: true,
      server: {
        auto_reconnect: true,
        socketOptions: {
          keepAlive: 1,
          connectTimeoutMS: 30000
        }
      },
      replset: {
        socketOptions: {
          keepAlive: 1,
          connectTimeoutMS: 30000
        }
      }
    });
});
mongoose.connect(dbURI, {
  server: {
    useMongoClient: true,
    auto_reconnect: true
  }
});

mongoose.Promise = require('bluebird');

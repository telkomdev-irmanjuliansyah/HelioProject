const Quota = require('../models/Quota');

var quotaLogic = {
  findAllQuota: function(callback) {
    Quota.find(function(err, quotas) {
      if (err) {
        return callback(err);
      }

      callback(quotas);
    });
  },
  saveQuota: function(params, callback) {
    var quota = new Quota({
      quota_name: params.quota_name,
      quota_price: params.quota_price,
      quota_description: params.quota_description
    });

    quota.save(function(err, quotaS) {
      if (err) {
        return callback(err);
      }
      callback(quotaS);
    });
  },

  editQuota: function(id, params, callback) {
    Quota.findById(id, function(err, quota) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      quota.quota_name = params.quota_name;
      quota.quota_price = params.quota_price;
      quota.quota_description = params.quota_description;

      quota.save();

      callback({
        status: true,
        message: 'Perubahan data berhasil.'
      });
    });
  },

  deleteQuota: function(id, callback) {
    Quota.remove({
      _id: id
    }, function(err, quota) {
      if (err) {
        callback({
          status: false,
          message: err
        });
        return;
      }

      callback({
        status: true,
        message: 'Quota berhasil dihapus.'
      });
    });
  }
};

module.exports = quotaLogic;
